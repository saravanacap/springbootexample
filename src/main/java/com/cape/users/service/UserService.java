package com.cape.users.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cape.users.dto.LoginDto;
import com.cape.users.dto.RegisterDto;
import com.cape.users.dto.UserDto;
import com.cape.users.entity.UserEntity;

import antlr.collections.List;
import at.favre.lib.crypto.bcrypt.BCrypt;

@Service
public class UserService {

	@Autowired
	com.cape.users.repository.UserRepository userRepository;
	private BCrypt passwordEncoder;

	public RegisterDto userRegister(RegisterDto user) {
		UserEntity userSave = new UserEntity();
		userSave.setEmail(user.getEmail());
		userSave.setFirstName(user.getFirstName());
		userSave.setLastName(user.getLastName());
//		  String hash  = passwordEncoder.with(BCrypt.Version.VERSION_2B).hash(4, );
		userSave.setPassword(user.getPassword());
		UserEntity savedUser = userRepository.save(userSave);
		RegisterDto userDto = new RegisterDto();
		userDto.setEmail(savedUser.getEmail());
		userDto.setFirstName(savedUser.getFirstName());
		userDto.setLastName(savedUser.getLastName());
		userDto.setPassword(savedUser.getPassword());
		return user;
	}

	public Boolean  login(LoginDto data) {

		UserEntity user = userRepository.findByEmail(data.getEmail());
		UserDto userDto = new UserDto();

		if (!user.getPassword().equals(data.getPassword())) {
			return false;
		}
		userDto.setEmail(user.getEmail());
		userDto.setFirstName(user.getFirstName());
		userDto.setLastName(user.getLastName());
		userDto.setId(user.getId());

		return true;
	}
}