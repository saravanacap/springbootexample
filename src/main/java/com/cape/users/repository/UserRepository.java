package com.cape.users.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.cdi.JpaRepositoryExtension;

import com.cape.users.dto.RegisterDto;
import com.cape.users.entity.UserEntity;

import antlr.collections.List;

public interface UserRepository extends JpaRepository<UserEntity, Long>{
	UserEntity findByEmail(String email);
	UserEntity findByPassword(String password);

}
