package com.cape.users.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cape.users.dto.LoginDto;
import com.cape.users.dto.RegisterDto;
import com.cape.users.dto.UserDto;
import com.cape.users.service.UserService;

@RestController
@RequestMapping("/api/v1/")
public class UserController {

	@Autowired
	UserService userService;

	@PostMapping("register")
	public ResponseEntity<RegisterDto> registerUser(@RequestBody RegisterDto user) {
		RegisterDto savedUser = userService.userRegister(user);
		return new ResponseEntity<RegisterDto>(savedUser, HttpStatus.CREATED);

	}

	@PostMapping("login")
	public ResponseEntity<Map<String, String>> loginUser(@RequestBody LoginDto data) {
		Boolean isLoggedIn = userService.login(data);
		Map response = new HashMap<>();
		response.put("message", "success");
		System.out.println(isLoggedIn);
		if (isLoggedIn) {
			return new ResponseEntity(response, HttpStatus.FOUND);
		}
		response.put("message", "Invalid credentials.");
		return new ResponseEntity(response, HttpStatus.NOT_FOUND);

	}

}
